require 'minitest/autorun'
require 'xoshiro256plusplus'

class TestXoshiro256plusplus < Minitest::Test
  def test_xoshiro256plusplus
    assert_equal 41131127393484853, Xoshiro256plusplus.next()
  end
end
