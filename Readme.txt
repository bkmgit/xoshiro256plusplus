This is an implementation of the Xoshiro256++
random number generation algorithm as a 
Ruby Gem. The algorithm is described at:

https://prng.di.unimi.it/

This is still work in progress, contributions
are welcome.

You can build and install the gem as follows:

git clone https://gitlab.com/bkmgit/xoshiro256plusplus
cd xoshiro256plusplus
rake compile
rake test
gem build xoshiro256plusplus.gemfile
gem install xoshiro256plusplus-0.0.1.gem

You can then generate random 64 bit integers in irb as follows

irb>require 'xoshiro256plusplus'
irb>Xoshiro256plusplus.next()

At present a fixed seed is used, but this will be updated to allow
the user the option of specifying the seed. 
